﻿Imports System.Runtime.InteropServices

Module SCLibrary
    #Region "Function Imports"
    <DllImport("SCLibrary.dll", CallingConvention:=CallingConvention.StdCall)> _
    Private Function SCHook (ByVal mfcHook As UInt32) As Int32
    End Function
    <DllImport("SCLibrary.dll", CallingConvention:=CallingConvention.StdCall)> _
    Private Function SCStaticInitializer () As Long
    End Function
    <DllImport("SCLibrary.dll", CallingConvention:=CallingConvention.StdCall)> _
    Private Function SCVersion () As String
    End Function
    <DllImport("SCLibrary.dll", CallingConvention:=CallingConvention.StdCall)> _
    Private Function SCInstanciate (ByRef devConf As DeviceConfig) As UIntPtr
    End Function
    <DllImport("SCLibrary.dll", CallingConvention:=CallingConvention.StdCall)> _
    Private Function SCInitialize(ByVal scHandle As UIntPtr) As Long
    End Function
    <DllImport("SCLibrary.dll", CallingConvention:=CallingConvention.StdCall)> _
    Private Function SCConfigure (ByVal scHandle As UIntPtr, ByRef conf As Config) As Int32
    End Function
    <DllImport("SCLibrary.dll", CallingConvention:=CallingConvention.StdCall)> _
    Private Function SCDelete (ByVal scHandle As UIntPtr) As Long
    End Function
    <DllImport("SCLibrary.dll", CallingConvention:=CallingConvention.StdCall)> _
    Private Function SCErrorCode (ByVal scHandle As UIntPtr, ByVal reset As Byte) As Int32
    End Function
    <DllImport("SCLibrary.dll", CallingConvention:=CallingConvention.StdCall)> _
    Private Function SCStartProcessing (ByVal scHandle As UIntPtr, ByVal block As Byte) As Int32
    End Function
    <DllImport("SCLibrary.dll", CallingConvention:=CallingConvention.StdCall)> _
    Private Function SCStopProcessing (ByVal scHandle As UIntPtr) As Int32
    End Function
    <DllImport("SCLibrary.dll", CallingConvention:=CallingConvention.StdCall)> _
    Private Function SCDocumentKeyAndStringSize (ByVal scHandle As UIntPtr, ByVal docId As UInt32, ByVal offset As UInt32, ByRef keySize As UInt32, ByRef outVal As UInt32) As UInt32
    End Function
    <DllImport("SCLibrary.dll", CallingConvention:=CallingConvention.StdCall)> _
    Private Function SCDocumentKeyAndString (ByVal scHandle As UIntPtr, ByVal docId As UInt32, ByVal offset As UInt32, ByVal outKey As System.Text.StringBuilder, ByVal outVal As System.Text.StringBuilder) As UInt32
    End Function
    #End Region
    #Region "Data types"
    Public Delegate Function ProcessingStartedCallback          (userObject As UIntPtr) As UInt32
    Public Delegate Function ProcessingFinishedCallback         (userObject As UIntPtr) As UInt32
    Public Delegate Function NewDocumentCallback                (userObject As UIntPtr, docId As UInt32, code As UInt32, name As String, doc As UIntPtr) As UInt32
    Public Delegate Function DocumentRecognitionDoneCallback    (userObject As UIntPtr, docId As UInt32, stringCount As UInt32, timestamp As String, doc As UIntPtr) As UInt32
    Public Enum ErrorCode
        ErrorCodeNoError                = 0
        ErrorCodeDeviceBadConfig
        ErrorCodeDeviceNotFound
        ErrorCodeDeviceJammed
        ErrorCodeFeedingFailure
        ErrorCodeIncompatibleDevice
        ErrorCodeLibraryLoadingFailure
        ErrorCodeLibraryFunctionFailure
        ErrorCodeStartScanFailure
        ErrorCodeNoPaper
        ErrorCodeMicrReadingFailure
        ErrorCodeOcrReadingFailure
        ErrorCodeNoCartridge
        ErrorCodeUnknownError
    End Enum
    Public Enum DeviceIdentifier
        DeviceIdentifierCanonCR55		= 0
        DeviceIdentifierCanonCR180		= 1
        DeviceIdentifierCanonCR190		= 2
        DeviceIdentifierEpsonTMS1000	= 3
        DeviceIdentifierPaniniVisionX	= 4
        DeviceIdentifierPaniniIDeal		= 5
        DeviceIdentifierEpsonTMS2000	= 6
        DeviceIdentifierPaniniVisionS   = 7
        DeviceIdentifierPaniniWIDeal	= 8
        DeviceIdentifierDummy			= 255
    End Enum
    Public Enum CallbackReturnCode
        CallbackReturnCodeReject        = 0
        CallbackReturnCodeAccept        = 1
        CallbackReturnCodeContinue      = 2
    End Enum
    Public Enum DocumentSide
            DocumentSideNone				= 0
            DocumentSideFront				= 1
            DocumentSideRear				= 2
            DocumentSideBoth				= 3
    End Enum
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi, Pack:=1)> _
    Public Structure DeviceConfig
        Public enableLogging As Byte
        Public deviceId As Byte
        <MarshalAs(UnmanagedType.LPStr)> Public dataPath As String
        <MarshalAs(UnmanagedType.LPStr)> Public locale As String
    End Structure
    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi, Pack:=1)> _
    Public Structure Config
        ' MICR options members.
        Public enableMicr As Byte
        ' Image options members.
        Public sides As Byte
        Public reversedDocument As Byte
        Public preferedResolution As UInt32
        Public enableColor As Byte
        Public enableImagesSaving As Byte
        ' Misc options members.
        Public dropFeedEnabled As Byte
        Public machineUid As UInt32
        Public enableDocUidReset As Byte
        ' Tests / Recognition.
        Public enableRejectionTests As Byte
        Public enableDoubleFeedingDetection As Byte
        Public enableRecognition As Byte
        Public enableProprietaryRecognition As Byte
        <MarshalAs(UnmanagedType.LPStr)> Public recognitionDataPath As String
        ' Misc.
        Public enableResultFilesSplit As Byte
        Public maximumDocumentCount As UInt32
        ' Print.
        <MarshalAs(UnmanagedType.LPStr)> Public printData As String
        <MarshalAs(UnmanagedType.LPStr)> Public printImageFilename As String
        Public printInBold As Byte
        Public printOffset As UInt32
        Public printImageOffset As UInt32
        ' Callbacks.
        Public userObject As UIntPtr
        Public onProcessingStarted As ProcessingStartedCallback
        Public onProcessingFinished As ProcessingFinishedCallback
        Public onNewDocument As NewDocumentCallback
        Public onDocumentRecognitionDone As DocumentRecognitionDoneCallback
    End Structure
    #End Region
    #Region "Static Methods"
    Public Function globalInitialization () As String
        Static bSIDone As Boolean       = False
        If (Not bSIDone)
            bSIDone                     = True
            SCHook                      (1)
            SCStaticInitializer         ()
        End If
        Return                          SCVersion()
    End Function
    #End Region
    #Region "Helper Class"
    Public Class Helper
        #Region "Delegates"
        Public Delegate Function ProcessingStartedCallback () As UInt32
        Public Delegate Function ProcessingFinishedCallback () As UInt32
        Public Delegate Function NewDocumentCallback (docId As UInt32, code As UInt32, name As String) As UInt32
        Public Delegate Function DocumentRecognitionDoneCallback (docId As UInt32, stringCount As UInt32, timestamp As String, strings As Dictionary(Of String, String)) As UInt32
        #End Region
        #Region "Public Members"
        Public deviceConfig                     As DeviceConfig
        Public config                           As Config
        Public onProcessingStarted              As ProcessingStartedCallback
        Public onProcessingFinished             As ProcessingFinishedCallback
        Public onNewDocument                    As NewDocumentCallback
        Public onDocumentRecognitionDone        As DocumentRecognitionDoneCallback
        #End Region
        #Region "Private Members"
        Private Shared instances                As Dictionary(Of UIntPtr, SCLibrary.Helper)        
        Private scHandle                        As UIntPtr      = 0
        Private bInitialized                    As Boolean      = False
        Private bConfigured                     As Boolean      = False
        #End Region
        #Region "Getters"
        Public ReadOnly Property instanciated   As Boolean
            Get
                Return scHandle<>0
            End Get
        End Property
        Public ReadOnly Property handle         As UIntPtr
            Get
                Return scHandle
            End Get
        End Property
        Public ReadOnly Property initialized    As Boolean
            Get
                Return bInitialized
            End Get
        End Property
        Public ReadOnly Property configured     As Boolean
            Get
                Return bConfigured
            End Get
        End Property
        #End Region
        #Region "Methods"
        ' Constructor.
        Public Sub New ()
            If (instances Is Nothing)
                instances               = New Dictionary(Of UIntPtr, SCLibrary.Helper)
            End If
            globalInitialization        ()
        End Sub
        ' Instanciation method.
        Public Function  instanciate () As Boolean
            If (scHandle)
                Throw New Exception(    "Instanciation cannot be achieved as the controller has already been instanciated.")
            End If
            scHandle                    = SCInstanciate(deviceConfig)
            instances.Add               (scHandle, Me)
            Return                      instanciated
        End Function
        ' Initialization method.
        Public Function initialize () As Boolean
            If (Not instanciated)
                Throw New Exception     ("Initialization can only occur on instanciated controllers.")
            ElseIf (bInitialized)
                Throw New Exception     ("Initialization can only occur once for any given controller.")
            End If
            bInitialized                = SCInitialize(scHandle)
            Return                      bInitialized
        End Function
        ' Configuration method.
        Public Function configure () As Boolean
            If (Not instanciated Or Not initialized)
                Throw New Exception     ("Configuration can only occur on instanciated and initializedcontrollers.")
            End If
            config.userObject           = scHandle
            config.onProcessingStarted          = AddressOf SCLibrary.Helper._onProcessingStarted
            config.onProcessingFinished         = AddressOf SCLibrary.Helper._onProcessingFinished
            config.onNewDocument                = AddressOf SCLibrary.Helper._onNewDocument
            config.onDocumentRecognitionDone    = AddressOf SCLibrary.Helper._onDocumentRecognitionDone
            bConfigured                 = SCConfigure(scHandle, config)
            Return                      bConfigured
        End Function
        
        ' Preparation method.
        Public Function prepare () As Boolean
            If ((scHandle=0 And Not instanciate()))
                Throw New Exception     ("Instanciation has failed.")
            Else If (Not bInitialized and Not initialize())
                Throw New Exception     ("Initialization has failed.")
            End If
            Return                      configure()
        End Function

        ' Start processing method.
        Public Function startProcessing (block As Boolean) As Boolean
            If (Not instanciated Or Not initialized Or Not configured)
                Throw New Exception     ("Impossible to start processing, as the controller is either not instanciated, initialized or configured.")
            End If
            Return                      SCStartProcessing(scHandle, block)
        End Function
        ' Stop processing method.
        Public Function stopProcessing () As Boolean
            Return                      SCStopProcessing(scHandle)
        End Function
        ' Last error retrieval method.
        Public Function errorCode () As ErrorCode
            Return If(scHandle=0, ErrorCode.ErrorCodeNoError, SCErrorCode(scHandle, 1))
        End Function

        ' Release method.
        Public Function release ()
            SCDelete                    (scHandle)
            instances.Remove            (scHandle)
            scHandle                    = 0
            bInitialized                = False
            bConfigured                 = False
            Return True
        End Function
        #End Region
        #Region "Static Callbacks"
        Private Shared Function _onProcessingStarted (userObject As UIntPtr) As UInt32
            Dim cb As ProcessingStartedCallback = instances.Item(userObject).onProcessingStarted
            If (cb IsNot Nothing) Then Return cb.Invoke()
            Return 0
        End Function
        Private Shared Function _onProcessingFinished (userObject As UIntPtr) As UInt32
            Dim cb As ProcessingFinishedCallback = instances.Item(userObject).onProcessingFinished
            If (cb IsNot Nothing) Then Return cb.Invoke()
            Return 0
        End Function
        Private Shared Function _onNewDocument (userObject As UIntPtr, docId As UInt32, code As UInt32, name As String, doc As UIntPtr) As UInt32
            Dim cb As NewDocumentCallback = instances.Item(userObject).onNewDocument
            If (cb IsNot Nothing) Then Return cb.Invoke(docId, code, name)
            Return CallbackReturnCode.CallbackReturnCodeContinue
        End Function
        Private Shared Function _onDocumentRecognitionDone (userObject As UIntPtr, docId As UInt32, stringCount As UInt32, timestamp As String, doc As UIntPtr) As UInt32
            Dim this                            = instances.Item(userObject)
            Dim cb As DocumentRecognitionDoneCallback = this.onDocumentRecognitionDone
            If (stringCount=0 Or cb Is Nothing)
                Return CallbackReturnCode.CallbackReturnCodeContinue
            End If
            
            Dim strings                         = New Dictionary(Of String, String)
            Dim keySize                     As UInt32
            Dim valSize                     As UInt32
            Dim key                         = New System.Text.StringBuilder("")
            Dim val                         = New System.Text.StringBuilder("")
            For i As UInt32=0 to stringCount-1
                If (SCDocumentKeyAndStringSize(this.scHandle, docId, i, keySize, valSize)<>0)
                    key.Length                  = keySize
                    val.Length                  = valSize
                    If (SCDocumentKeyAndString(this.scHandle, docId, i, key, val)<>0) Then strings.Add(key.ToString(), val.ToString())
                End If
            Next
            Return cb.Invoke(docId, stringCount, timestamp, strings)
        End Function
        #End Region
    End Class
    #End Region
End Module
