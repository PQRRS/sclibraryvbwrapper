﻿Imports System.Runtime.InteropServices

Module MainModule
    Sub Main ()
        Console.Write                           ("Press a key to begin...")
        Console.ReadLine                        ()
        Console.WriteLine                       ("Initializing library with version: {0}", SCLibrary.globalInitialization())
        
        Dim sch As SCLibrary.Helper             = New SCLibrary.Helper
        sch.onProcessingStarted                 = AddressOf onProcessingStarted
        sch.onProcessingFinished                = AddressOf onProcessingFinished
        sch.onNewDocument                       = AddressOf onNewDocument
        sch.onDocumentRecognitionDone           = AddressOf onDocumentRecognitionDone

        Console.WriteLine                       ("Configuring device...")
        ' This is the generic parameters and device selection options.
        sch.deviceConfig.enableLogging          = 1
        sch.deviceConfig.deviceId               = SCLibrary.DeviceIdentifier.DeviceIdentifierDummy
        sch.deviceConfig.dataPath               = "C:/Results"
        sch.deviceConfig.locale                 = "EN"
        ' Those are the pass configuration options.
        sch.config.enableMicr                   = 1
        sch.config.sides                        = 1
        sch.config.reversedDocument             = 0
        sch.config.preferedResolution           = 200
        sch.config.enableColor                  = 0
        sch.config.enableImagesSaving           = 0
        sch.config.dropFeedEnabled              = 0
        sch.config.machineUid                   = 0
        sch.config.enableDocUidReset            = 0
        sch.config.enableRejectionTests         = 1
        sch.config.enableDoubleFeedingDetection = 1
        sch.config.enableRecognition            = 1
        sch.config.enableProprietaryRecognition = 1
        sch.config.recognitionDataPath          = "CRT.recb"
        sch.config.enableResultFilesSplit       = 0
        sch.config.maximumDocumentCount         = 200
        sch.config.printData                    = ""
        sch.config.printImageFilename           = ""
        sch.config.printInBold                  = 0
        sch.config.printOffset                  = 45
        sch.config.printImageOffset             = 125
        ' Instanciation, Initialization and Configuration are all handled by this method.
        Console.WriteLine                       ("Preparing controller: {0}", sch.prepare())
        ' To start a processing pass, just call startProcessing. The parameter makes the call blocking or not.
        Console.WriteLine                       ("Starting processing: {0}", sch.startProcessing(True))
        ' Once the processing is over, the controller can either be re-used (And .configure() can be called once the configuration has changed),
        ' then as many passes can be started with the startProcessing method over and over again.
        ' Once the last processing pass has occured (Most likely when closing your application), you need to call .release
        ' to free resources and memory being used by the libraries chain.
        Console.WriteLine                       ("Releasing controller: {0}", sch.release())
        
        Console.Write                           ("Press a key to quit...")
        Console.ReadLine                        ()
    End Sub
    #Region "Callbacks"
        Public Function onProcessingStarted () As UInt32
            Console.WriteLine   ("onProcessingStarted")
            Return              0
        End Function
        Public Function onProcessingFinished () As UInt32
            Console.WriteLine   ("onProcessingFinished")
            Return              0
        End Function
        Public Function onNewDocument (docId As UInt32, code As UInt32, name As String) As UInt32
            Console.WriteLine   ("onNewDocument: {0}", name)
            Return              SCLibrary.CallbackReturnCode.CallbackReturnCodeContinue
        End Function
        Public Function onDocumentRecognitionDone (docId As UInt32, stringCount As UInt32, timestamp As String, strings As Dictionary(Of String, String)) As UInt32
            For Each pair In strings
                Console.WriteLine   ("  {0}: {1}", pair.Key, pair.Value)
            Next
            Return SCLibrary.CallbackReturnCode.CallbackReturnCodeContinue
        End Function
    #End Region
End Module
